import {Sequelize} from "sequelize";
import os from "os"
import path from "path"
import Schema from "./schema";
import logger from "./logger";
import {Handler} from "./handler";
import fs from "fs";


class BaseModel {
    private readonly sqlize: Sequelize;
    private readonly relations: any;
    private readonly handler: Handler;

    constructor(handler: Handler) {
        this.handler = handler
        this.relations = {
            "init": {
                "hasOne": (A: any, B: any, options?: any) => A.hasOne(B, options),
                "hasMany": (A: any, B: any, options?: any) => A.hasMany(B, options),
                "belongsTo": (A: any, B: any, options?: any) => A.belongsTo(B, options),
                "belongsToMany": (A: any, B: any, options?: any) => A.belongsToMany(B, options),
            },
            "create": {
                "hasOne": this.created_hasOne.bind(this),
            }
        };
        this.sqlize = new Sequelize(
            {
                dialect: 'sqlite',
                storage: `${path.join(os.homedir(), ".im", "imstore", this.handler?.getOption("app"), this.handler?.getDate(), `${this.handler?.getDataBase()}.db`)}`,
                logging: (m) => logger.debug(`model constructor: ${m}`),
            }
        )
    }


    async init() {
        try {
            const params = new Schema(this.handler?.getOption("init")).build()
            let models: any = {}
            params?.forEach((params: any) => {
                const {entity, fields} = params;
                const model = this.sqlize?.define(entity, fields, {
                    freezeTableName: true
                });
                Object.assign(models, {[entity]: model})
            })
            params.forEach((param: { entity: string, relations: { [key: string]: any[] } }) => {
                const {entity, relations} = param
                if (relations) {
                    Object.keys(relations)?.forEach((relation: string) => {
                        if (Object.keys(this.relations?.init).includes(relation)) {
                            if (Array.isArray(relations[relation])) {
                                relations[relation].forEach((related: string) => {
                                    if (Object.keys(models).includes(entity) &&
                                        Object.keys(models).includes(related)) {
                                        let relation_table = null
                                        if (["belongsToMany"].includes(relation)) {
                                            relation_table = this.sqlize?.define(`${entity}_${related}`, {});
                                        }
                                        this.relations?.init[relation](models[entity], models[related], {through: relation_table})
                                        logger.debug(`init relation: ${entity} ${relation} ${related}`)
                                    }
                                })
                            }
                        }
                    })
                }
            })
            await this.sqlize?.sync()
            console.log({"status": 1})
            logger.debug("init: Database init success!")
        } catch (e: any) {
            logger.error(`init: ${e.message}`)
        }
    }

    async create() {
        try {
            const data = this.handler.getData()
            const relations = this.handler.getRelations()
            const entity_name: string = this.handler.getOption("entity")
            logger.debug(`Creating ${entity_name} item ...`)
            logger.debug(`${entity_name} data: ${data}`)
            const query = this.sqlize?.getQueryInterface()
            const entity_id = await query?.bulkInsert(entity_name, [data]);
            if (Object.keys(relations).length > 0) {
                for (const rel of Object.keys(relations)) {
                    await this.relations["create"][rel](query, entity_name, entity_id, relations[rel])
                }
            }
            console.log({"status": 1})
        } catch (e: any) {
            logger.error(e.message)
        }
    }

    async created_hasOne(query: any, entity_name: string, entity_id: number, relations: any) {
        try {
            if (Object.keys(relations).length > 0) {
                for (const relation of Object.keys(relations)) {
                    const relation_data = relations[relation]
                    if (relation_data) {
                        relation_data[`${entity_name}Id`] = entity_id
                        await query.bulkInsert(relation, [relation_data]);
                    }
                }
            }
        } catch (e: any) {
            logger.error(e.message)
        }
    }

    async update() {
        try {

            const values = this.handler.getData()
            const identifiers = this.handler.getCriteria()
            const entity_name: string = this.handler.getOption("entity")

            logger.debug(`Updating ${entity_name} item ...`)
            logger.debug(`${entity_name} values: ${JSON.stringify(values)}`)
            const query = this.sqlize?.getQueryInterface()
            const data = await query?.bulkUpdate(entity_name, values, identifiers);
            logger.debug(data)
            console.log(data)
        } catch (e: any) {
            logger.error(e.message)
        }
    }

    async fetch() {
        try {
            const criteria = this.handler.getCriteria()
            const entity_name: string = this.handler.getOption("entity")
            logger.debug(`Fetching ${entity_name} item ...`)
            logger.debug(`${entity_name} where: ${JSON.stringify(criteria)}`)
            const query = this.sqlize?.getQueryInterface()
            const data = await query?.select(null, entity_name, {where: criteria});
            logger.debug(data)
            console.log(data)
        } catch (e: any) {
            logger.error(e.message)
            throw new Error(e)
        }
    }

    async remove() {
        try {
            const criteria = this.handler.getCriteria()
            const entity_name: string = this.handler.getOption("entity")
            logger.debug(`Removing ${entity_name} item ...`)
            logger.debug(`${entity_name} where: ${JSON.stringify(criteria)}`)
            const query = this.sqlize?.getQueryInterface()
            const data = await query?.bulkDelete(entity_name, criteria);
            logger.debug(data)
            console.log(data)
        } catch (e: any) {
            console.trace(e)
            logger.error(e.message)
        }
    }
}

export default BaseModel