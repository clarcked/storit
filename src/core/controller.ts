import {Handler} from "./handler";
import logger from "./logger";
import BaseModel from "./model";


class Controller {
    private readonly actions: { [key: string]: Function } = {
        "init": this.init.bind(this),
        "create": this.create.bind(this),
        "update": this.update.bind(this),
        "fetch": this.fetch.bind(this),
        "remove": this.remove.bind(this),
    }

    private readonly handler: Handler

    constructor(handler: Handler) {
        this.handler = handler
    }

    execute() {
        try {
            const action = this.handler.getAction()
            this.actions[action]()
        } catch (e: any) {
            logger.debug(e.message)
            throw new Error(e)
        }
    }

    async init() {
        try {
            const model = new BaseModel(this.handler)
            await model.init()
        } catch (e: any) {
            logger.error(e.message)
        }
    }

    async create() {
        try {
            const model = new BaseModel(this.handler)
            await model.create()
        } catch (e: any) {
            logger.error(e.message)
        }
    }

    async update() {
        try {
            const model = new BaseModel(this.handler)
            await model.update()
        } catch (e: any) {
            logger.error(e.message)
        }
    }

    async fetch() {
        try {
            const model = new BaseModel(this.handler)
            await model.fetch()
        } catch (e: any) {
            logger.error(e.message)
            throw new Error(e)
        }
    }

    async remove() {
        try {
            const model = new BaseModel(this.handler)
            await model.remove()
        } catch (e: any) {
            logger.error(e.message)
        }
    }
}


export default Controller