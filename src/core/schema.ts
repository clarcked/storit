import {DataTypes} from "sequelize";
import fs from "fs";
import logger from "./logger";

class Schema {
    private readonly spec: any;
    private readonly types: { [key: string]: any } = {
        "integer": DataTypes.INTEGER,
        "bigint": DataTypes.BIGINT,
        "uuid": DataTypes.UUID,
        "float(11)": DataTypes.FLOAT(11),
        "string": DataTypes.STRING,
        "text": DataTypes.TEXT,
        "array": DataTypes.ARRAY,
        "datetime": DataTypes.DATE,
        "date": DataTypes.DATEONLY,
        "float": DataTypes.FLOAT,
    }

    constructor(filename: string) {
        try {
            let raw = fs.readFileSync(filename, "utf-8")
            this.spec = JSON.parse(raw, this.hydrator.bind(this))
        } catch (e: any) {
            logger.debug(`schema constructor: ${e.message}`)
        }
    }

    hydrator(key: any, val: any) {
        if (key == "type") {
            val = this.types[val]
        }
        return val
    }

    build() {
        const {resources} = this.spec;
        return resources
    }

    version() {
        return this.spec?.version
    }

    app() {
        return this.spec?.meta?.application
    }

    manager() {
        return this.spec?.meta?.manager
    }
}


export default Schema