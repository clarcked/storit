export interface OptionInterface {
    description: string;
    alias: string;
    type: string
    build: Function
}
