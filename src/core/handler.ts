import moment from "moment";
import logger from "./logger";

export class Handler {
    private readonly cmd: Array<string>;
    private readonly opts: any;

    constructor(cmd: Array<string>, options: any) {
        this.cmd = cmd
        this.opts = options
    }

    validateCmd() {
        if (!Array.isArray(this.cmd)) {
            throw new Error(`Expecting an Array as command list`)
        }
        if (this.cmd.length > 1) {
            let bad = ""
            this.cmd.forEach((cmd) => {
                bad = `${bad} ${cmd}`
            })
            throw new Error(`Invalid ${bad} command(s)`)
        }
    }

    getCriteria() {
        const data = this.getOption("criteria")
        return this.getFields(data)
    }

    getData(): { [key: string]: any } {
        const data = this.getOption("data")
        let fields = this.getFields(data)
        return {...fields, "createdAt": new Date().toJSON(), "updatedAt": new Date().toJSON()}
    }

    getRelations(): { [key: string]: any } {
        const data = this.getOption("relations")
        let relations: { [key: string]: any } = {}
        if (Array.isArray(data)) {
            data.forEach((v: any) => {
                this.getExtractFields(v, relations)
            })
        } else {
            this.getExtractFields(data, relations)
        }
        return relations
    }

    getExtractFields(arg: any, relations: { [key: string]: any }) {
        if (arg) {
            let split = arg.split(":")
            let rel_type: string = split.shift().replace(new RegExp("\{", 'g'), "")
            //if (["hasOne", "hasMany", "belongsTo"].includes(rel_type)) {
            if (["hasOne"].includes(rel_type)) {
                let entity_name: string = split.shift().replace(new RegExp("\{", 'g'), "")
                let sub: { [key: string]: string } = {}
                relations[rel_type] = {}
                split.forEach((r: string, i: number) => {
                    if ((i % 2) == 0) {
                        const key = r.replace(new RegExp("\{", 'g'), "")
                        sub[key] = split[i + 1].replace(new RegExp("\}", 'g'), "")
                        sub['createdAt'] = (new Date()).toJSON()
                        sub['updatedAt'] = (new Date()).toJSON()
                    }
                })
                if (relations) {
                    relations[rel_type][entity_name] = sub
                }
            }
        }
    }

    getFields(data: any) {
        const fields: { [key: string]: string } = {}
        const extractField = (v: string) => {
            let field = v.split(":")
            if (field.length == 2) {
                const key = field[0].replace(new RegExp("\{"), "")
                fields[key] = field[1].replace(new RegExp("\}"), "")
            }
        }
        if (Array.isArray(data)) {
            data.forEach(extractField)
        } else if (typeof data === "string") {
            extractField(data)
        }
        return fields
    }

    getDate(): string {
        let date = moment().format("YYYYMM")
        try {
            date = moment(this.getOption("date")).format("YYYYMM")
        } catch (e: any) {
            logger.debug(e.message)
        }
        return date
    }

    getDataBase() {
        return this.opts["database"] ? this.opts['database'] : "data"
    }

    getOption(key: string) {
        return this.opts[key]
    }

    getAction() {
        this.validateCmd()
        return this.cmd[0]
    }
}