import yargs from "yargs"
import {Handler} from "./core/handler";
import logger from "./core/logger";
import Controller from "./core/controller";

const argv: any = yargs
    .command('init', 'Data schema initializer', {})
    .command('create', 'Create an entity', {})
    .command('edit', 'Update an entity', {})
    .command('remove', 'Remove an entity', {})
    .command('fetch', 'Search for an entity by a key', {})
    .option('app', {
        alias: 'a',
        description: 'Describe the application name we are working on.',
    })
    .option('database', {
        alias: 'b',
        description: 'Describe the database name we are working on.',
    })
    .option('date', {
        alias: 't',
        description: 'Describe the current date we are working on.',
    })
    .option('entity', {
        alias: 'e',
        description: 'Describe the entity we are working on.',
    })
    .option('data', {
        alias: 'd',
        description: 'The processing data as a json stringified.',
    }).option('relations', {
        alias: 'r',
        description: 'The associated entity data as a json stringified.',
    }).option('criteria', {
        alias: 'c',
        description: 'The criteria of search as a json stringified.',
    })
    .option('init', {
        alias: 'i',
        description: 'Init config as json stringified.',
    })
    .help()
    .alias('help', 'h')
    .argv;
logger.debug(argv)
try {
    const controller = new Controller(new Handler(argv._, argv))
    controller.execute()
} catch (e: any) {
    logger.debug(e.message)
    throw new Error(e)
}